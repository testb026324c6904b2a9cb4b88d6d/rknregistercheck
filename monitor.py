from rkn_url_db import UrlTableRow
from rkn_url_db import Database
from dns.resolver import NoAnswer
from urllib3.exceptions import ConnectTimeoutError, MaxRetryError
from urllib.parse import quote, urlsplit
from rkn_dump_parser.parser_class import Parser
from queue import Queue
import threading
import dns.resolver
import urllib3
import logging
from util import ip2int
from _mysql_exceptions import OperationalError
from urllib3 import Retry, Timeout
import time
from profiling import Stats
import re
from settings import db


class Monitor:
    n = 0
    errors = 0
    urls_count = 0
    urls = []
    dump_urls = Queue()
    prepared_urls = Queue()
    responses = Queue()
    rows = Queue()
    # type: Database
    db = None

    def __init__(self):
        self.db = Database(user=db.username, password=db.password, db=db.db_name)
        self.lock = threading.RLock()
        self.db_lock = threading.RLock()
        self.num_threads = 55
        self.db.clear()

        for i in range(self.num_threads):
            lookup_thread = threading.Thread(target=self.lookup, name='lookup{}'.format(i))
            lookup_thread.setDaemon(True)
            lookup_thread.start()
            request_thread = threading.Thread(target=self.do_request, name='request{}'.format(i))
            request_thread.setDaemon(True)
            request_thread.start()
            analyzer_thread = threading.Thread(target=self.analyze_response, name='analyzer{}'.format(i))
            analyzer_thread.setDaemon(True)
            analyzer_thread.start()

    def run(self, parser: Parser) -> None:
        """
        TODO: Сделать какой-нибудь локальный кэш, чтоб не резолвить один и тот же хост.
        :param parser: see @Parser
        """
        for urls in parser.urls(with_ip=True):
            for url in urls:
                self.n += 1
                self.urls_count += 1
                if isinstance(url, list):
                    for i in url:
                        self.prepared_urls.put(i)
                else:
                    self.prepared_urls.put(url)

    def run_with_resolve(self, parser: Parser) -> None:
        """
        TODO: Сделать какой-нибудь локальный кэш, чтоб не резолвить один и тот же хост.
        :param parser: see @Parser
        """
        # self.dump_urls.put("http://ya.ru/")
        for urls in parser.urls(with_ip=False):
            for url in urls:
                self.n += 1
                self.urls_count += 1
                if isinstance(url, list):
                    for i in url:
                        self.dump_urls.put(i)
                else:
                    self.dump_urls.put(url)

    def lookup(self):
        my_resolver = dns.resolver.Resolver()
        my_resolver.nameservers = ['94.26.128.3', '94.26.128.4']
        while True:
            url = self.dump_urls.get()
            parse_result = urlsplit(url=url)
            try:
                "DNS-query at {}".format(parse_result.netloc.encode('idna').decode('utf8'))
                domain = parse_result.netloc.encode('idna').decode('utf8').split(':')[0]
                if len(re.sub('[.0-9]*', '', domain)) == 0:
                    self.prepared_urls.put((url, domain))
                else:
                    for ip in my_resolver.query(domain, "A"):
                        self.prepared_urls.put((url, ip))
            except NoAnswer:
                logging.error("DNS-server didn't answered at {}".format(parse_result.netloc))
            except dns.resolver.NXDOMAIN as e:
                logging.error("NXDOMAIN: {} at {}".format(e, parse_result.netloc))
            except dns.resolver.NoNameservers as e:
                logging.error("{} at {}".format(e, parse_result.netloc))
            except dns.resolver.Timeout:
                logging.error("DNS timeout at {}".format(parse_result.netloc))
            except TypeError as e:
                logging.error("TypeError: {} at {}".format(e, parse_result.netloc))
            finally:
                self.dump_urls.task_done()

    def do_request(self):
        parse_dict = []
        with urllib3.PoolManager(cert_reqs='CERT_NONE') as http:
            while True:
                url, ip = self.prepared_urls.get()
                try:
                    parse_result = urlsplit(url=url)
                    parse_dict = dict(parse_result._asdict())
                    if not parse_result.scheme.startswith('http'):
                        continue
                    parse_dict['netloc'] = "{}".format(ip)
                    parse_dict['path'] = quote(parse_result.path)\
                        .replace('%25', '%') \
                        .replace("%2C", ",") \
                        .replace("%2F", "/") \
                        .replace("%3F", "?") \
                        .replace("%3A", ":") \
                        .replace("%40", "@") \
                        .replace("%26", "&") \
                        .replace("%3D", "=") \
                        .replace("%2B", "+") \
                        .replace("%24", "$") \
                        .replace("%23", "#") \
                        .replace("%7E", "~") \
                        .replace("%28", "(") \
                        .replace("%29", ")") \
                        .replace("%5B", "[") \
                        .replace("%5D", "]") \
                        .replace('%21', '!') \
                        .replace('%22', '"')

                    parse_dict['query'] = quote(parse_dict.get('query', '')) \
                        .replace('%25', '%') \
                        .replace("%2C", ",") \
                        .replace("%3A", ":") \
                        .replace("%40", "@") \
                        .replace("%26", "&") \
                        .replace("%3D", "=") \
                        .replace("%2B", "+") \
                        .replace("%24", "$") \
                        .replace("%23", "#") \
                        .replace("%7E", "~") \
                        .replace("%28", "(") \
                        .replace("%29", ")") \
                        .replace("%5B", "[") \
                        .replace("%5D", "]") \
                        .replace('%21', '!') \
                        .replace('%22', '"')
                    new_url = parse_result._replace(**parse_dict).geturl()
                    headers = {
                        "Host": parse_result.netloc.encode("idna").decode("utf8"),
                        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1",
                        "Connection": 'close'
                    }
                    res = http.request("HEAD", new_url, headers=headers, retries=Retry(5, redirect=False),
                                       timeout=Timeout(connect=1.0, read=4.0))
                    self.responses.put((res, url, ip, new_url))
                except ConnectTimeoutError:
                    logging.error("Timeout at {}\n".format(url))
                except MaxRetryError:
                    logging.error("Max retry at {}\n".format(url))
                except UnicodeError as e:
                    logging.error("UnicodeError: {} at\n\t{}".format(e, url))
                except KeyError as e:
                    logging.error("KeyError '{}' at '{}'\n\t{}".format(e, url, parse_dict))
                except Exception as e:
                    logging.error("We have an error '{}' at '{}'\n\t{}".format(type(e), url, parse_dict))
                finally:
                    self.prepared_urls.task_done()

    def analyze_response(self):
        while True:
            res, url, ip, new_url = self.responses.get()
            row = UrlTableRow()
            row.url = url
            row.prepared_url = new_url
            row.http_status = res.status
            row.redirect = res.getheader("Location")
            row.ip = ip2int(ip)

            self.rows.put(row)
            self.responses.task_done()

    def write_to_db(self):
        while True:
            if self.rows.empty():
                break
            row = self.rows.get()
            try:
                if self.db is None:
                    raise Exception
                with Stats('monitor.write_to_db'):
                    self.db.write(row)
            except OperationalError as e:
                logging.error(e)
                pass
            finally:
                self.rows.task_done()

    def wait_4_end(self):
        start = time.time()
        self.dump_urls.join()
        logging.info("Parsing ended.")
        self.prepared_urls.join()
        logging.info("\nAll domains resolved.")
        logging.info("Time passed: {}".format(time.time() - start))
        for i in range(1):
            db_writer_thread = threading.Thread(target=self.write_to_db, name='db{}'.format(i))
            db_writer_thread.setDaemon(True)
            db_writer_thread.start()
        self.responses.join()
        logging.info("All url processed.")
        logging.info("Writing to database.")
        self.rows.join()
        self.write_to_db()
        logging.info("Time passed: {}".format(time.time() - start))
