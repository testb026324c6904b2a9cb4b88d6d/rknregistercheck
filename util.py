import socket
import struct
from functools import reduce
from settings import tg_bot
from http.client import HTTPSConnection, HTTPConnection
from urllib.parse import quote_plus
import hashlib
from datetime import datetime
from base64 import b64encode


def ip2int(address):
    if isinstance(address, str):
        ip = reduce(lambda res, x: res * 256 + x, map(lambda x: int(x), address.split('.')))
    else:
        address = socket.inet_aton(address.address)
        ip = struct.unpack("!I", address)[0]
    if ip == 2147483647:
        pass
    return ip


def int_to_ip(number):
    ip = None
    if isinstance(number, int):
        ip = socket.inet_ntoa(struct.pack("!I", number))
    return ip


def send_file_to_telegram(message):
    md5 = hashlib.md5()
    query_string = "parse_mode={}&chat_id={chat_id}".format("html", chat_id=tg_bot.chat_id)

    url = 'https://{domain}/bot{token}/sendDocument?{qs}'.format(
        domain=tg_bot.host,
        token=tg_bot.key,
        qs=query_string
    )
    md5.update(str(datetime.now().timestamp()).encode())
    boundary = md5.hexdigest()
    data = '\r\n' \
           '--' + boundary + '\r\n'\
           'Content-Disposition: form-data; name="document"; filename="zapret.csv"\r\n' \
           'Content-Type: text/csv\r\n' \
           '\r\n' + \
           message + '\r\n'
    data += "--" + boundary + "--\r\n\r\n"
    data = data.encode()
    cl = len(data)
    http_client = HTTPSConnection(tg_bot.host, '443')
    headers = {
        "Content-Type": "multipart/form-data; boundary=" + boundary,
        "Content-Length": cl
    }
    http_client.request("POST", url=url, headers=headers)
    http_client.send(data=data)
    http_client.getresponse()


def send_to_telegram(message):
    query_string = "parse_mode={}&chat_id={chat_id}&text={text}".format(
        "html", chat_id=tg_bot.chat_id, text=quote_plus(message)
    )

    url = 'https://{domain}/bot{token}/sendMessage?{qs}'.format(
        domain=tg_bot.host,
        token=tg_bot.key,
        qs=query_string
    )

    http_client = HTTPSConnection(tg_bot.host, '443')
    http_client.request("GET", url)
    http_res = http_client.getresponse()

    if http_res.status == 200:
        return True
    else:
        return False


def read_dump_from_http() -> bytes:
    from settings import dump
    http_client = HTTPConnection(dump.host, '80')
    headers = {
        "Authorization": "Basic " + b64encode("{}:{}".format(dump.user, dump.password).encode()).decode()
    }
    http_client.request("GET", dump.path, headers=headers)
    http_res = http_client.getresponse()
    if http_res.status == 200:
        dump = http_res.read()
    else:
        dump = b''
    return dump
