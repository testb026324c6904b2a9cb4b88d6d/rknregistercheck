import time


class UrlTableRow:

    def __init__(self,):
        self.url = ''
        self.prepared_url = ''
        self.ip = '0.0.0.0'
        self.http_status = -1
        self.redirect = None
        self.checked_at = time.time()

    def __str__(self):
        return "{" + "url: '{}', ip: '{}', status: {}, redirect: '{}'"\
            .format(self.url, self.ip, self.http_status, self.redirect) + "}"
