import MySQLdb
from rkn_url_db.rkn_url_table import UrlTableRow


class Database:
    def __init__(self, hostname="localhost", user="", password="", db='', port=3306):
        self.db = MySQLdb.connect(hostname, user, password, db, port, use_unicode=True, charset='utf8')
        pass

    def write(self, url: UrlTableRow) -> int:
        cursor = self.db.cursor()
        try:
            cursor.execute(
                "INSERT INTO `urls` "
                "(`url`, `ip`, `http_status`, `redirect`, `checked_at`, `prepared_url`) "
                "values (%s, %s, %s, %s, NOW(), %s)",
                (url.url, url.ip, url.http_status, url.redirect, url.prepared_url)
            )
        except UnicodeDecodeError:
            pass
        except Exception as e:
            raise e
        return self.db.affected_rows()

    def get_counters(self) -> list:
        cursor = self.db.cursor()
        cursor.execute("select count(id) from urls")
        total = cursor.fetchone()[0]
        cursor.execute(
            "select count(id) from urls where http_status<300 or redirect not like '%zapret.arbital.ru%'"
        )
        youtube = cursor.fetchone()[0]
        cursor.execute(
            'select count(id) from urls '
            'where (http_status<300 or redirect not like "%zapret.arbital.ru%") '
            'and url not like "%youtube%" '
            'and url not like "%youtu.be%"'
            ' AND url NOT LIKE "https%"'
        )
        no_youtube = cursor.fetchone()[0]
        return total, youtube, no_youtube

    def get_suspicious(self) -> str:
        message = ''
        cursor = self.db.cursor()
        num_rows = cursor.execute(
            'select id, url, INET_NTOA(ip), prepared_url, http_status, redirect, checked_at '
            'from urls '
            'where (http_status<300 or redirect not like "%zapret.arbital.ru%") '
            'AND url NOT LIKE "https%" '
            'ORDER BY url ASC'
        )
        if num_rows > 0:
            sep = '"; "'
            message = '"' + sep.join(['id', 'url', 'ip', 'prepared_url', 'http_status', 'redirect', 'checked_at']) + '"\n'
            for i in cursor.fetchall():
                i = list(map(str, i))
                message += '"' + sep.join(i) + '"\n'
        return message

    def clear(self):
        cursor = self.db.cursor()
        cursor.execute('TRUNCATE urls')
        self.db.commit()

    def commit(self):
        self.db.commit()

    def __del__(self):
        self.db.commit()
