from monitor import Monitor
from rkn_dump_parser import Parser


class Tst(Parser):
    def urls(self, with_ip=False) -> iter:
        to_test = [
            ['http://www.pixiv.net/member_illust.php?mode=medium&illust_id=46850176']
        ]
        for url in to_test:
            yield url

mon = Monitor()
tst = Tst()
mon.run(tst)
