# -*-*- coding: utf-8 -*-*-
from xml.etree import ElementTree
import logging
from settings.logging import log_filename

logging.captureWarnings(True)
logging.basicConfig(filename=log_filename, level=logging.ERROR)


class Parser:
    def __init__(self, data=''):
        self.xml_root = ElementTree.fromstring(data)

    def urls(self, with_ip=False) -> iter:
        for node in self.xml_root.getchildren():
            yield self.make_url(node, with_ip)

    @staticmethod
    def make_url(xml_node: ElementTree.Element, with_ip=False) -> iter:
        if "blockType" not in xml_node.attrib or xml_node.attrib['blockType'] == 'default':
            if len(xml_node.findall('url')) > 0:
                for url in xml_node.findall('url'):
                    if with_ip:
                        for ip in xml_node.findall('ip'):
                            yield (url.text, ip.text)
                    else:
                        yield url.text
                    # yield url.text
            elif len(xml_node.findall('domain')) > 0:
                for domain in xml_node.findall('domain'):
                    if with_ip:
                        for ip in xml_node.findall('ip'):
                            yield [("http://{}/".format(domain), ip), ("https://{}/".format(domain), ip)]
                    else:
                        yield ["http://{}/".format(domain), "https://{}/".format(domain)]
            elif len(xml_node.findall('ip')) > 0:
                for ip in xml_node.findall('ip'):
                    yield [("http://{}/".format(ip), ip), ("https://{}/".format(ip), ip)]
            else:
                logging.error("Нечто странное у объекта {}".format(xml_node.attrib['hash']))
                ElementTree.dump(xml_node)
                yield None
