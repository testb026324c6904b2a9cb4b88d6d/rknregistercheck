from rkn_dump_parser import Parser

import time
import logging
from monitor import Monitor
from util import send_to_telegram, send_file_to_telegram, read_dump_from_http
from settings.logging import log_filename


log = open(log_filename, 'w')
log.write('')
log.close()
start = time.time()
logging.captureWarnings(True)
logging.basicConfig(filename=log_filename, level=logging.ERROR)


def load_dump(filename: str):
    mon = Monitor()
    try:
        # with open(filename, encoding="cp1251") as dump_file:
        try:
            dump = read_dump_from_http()
            parser = Parser(dump.decode("cp1251"))
            mon.run_with_resolve(parser)
            mon.wait_4_end()
            if mon.db:
                mon.db.commit()
            logging.info("Done: {}s".format(time.time() - start))

            counters = mon.db.get_counters()
            if counters[2] > 0:
                suspicious = mon.db.get_suspicious()
                mon.db.get_counters()
                message = "<b>Всего записей проверено</b>: {},\n" \
                          "<b>Доступно (включая youtube)</b>: {}\n" \
                          "<b>Доступно (Без учёта youtube)</b>: {}".format(*counters)
                send_to_telegram(message)
                send_file_to_telegram(suspicious)

        except IOError as e:
            log_entry = "\tFailed to read file '{}':\n{}\n".format(filename, e.message)
            logging.error(log_entry)
            exit()
    except FileNotFoundError:
        log_entry = "\tFile not found: '{}'\n".format(filename)
        logging.error(log_entry)
        exit()
    except KeyboardInterrupt:
        exit(1)


if __name__ == "__main__":
    load_dump("dump.xml")

